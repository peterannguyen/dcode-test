    @extends('layouts.app')
    
    @section('content')
    <a href="{{ route('tasks.create') }}" class="btn btn-success mb-2">Add</a> 
    <br>
    <div class="row">
            <div class="col-12">
            
            <table class="table table-bordered" id="laravel_crud">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Created at</th>
                    <td colspan="2">Action</td>
                </tr>
            </thead>
            <tbody>
                @foreach($tasks as $task)
                <tr>
                    <td>{{ $task->name }}</td>
                    <td>{{ $task->description }}</td>
                    <td>{{ date('Y-m-d', strtotime($task->created_at)) }}</td>
                    <td><a href="{{ route('tasks.edit',$task->id)}}" class="btn btn-primary">Edit</a></td>
                    <td>
                    <form action="{{ route('tasks.destroy', $task->id)}}" method="post">
                    {{ csrf_field() }}
                    @method('DELETE')
                    <button class="btn btn-danger" type="submit">Delete</button>
                    </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
            </table>
            {!! $tasks->links() !!}
        </div> 
    </div>
    @endsection