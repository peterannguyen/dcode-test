<?php
namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\TaskStoreRequest;
use Redirect;
use PDF;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $userId = Auth::user()->id;
        $data['tasks'] = Task::orderBy('id','desc')->where('user_id',$userId)->paginate(10);
        return view('list',$data);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TaskStoreRequest $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
        ]);
        $task = new Task();
        $task->name = $request->name;
        $task->description = $request->description;
        $task->user_id = Auth::user()->id;
        $task->save();
        return Redirect::to('tasks')->with('success','Greate! Task created successfully.');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {

    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {   
        $data['task_info'] = $task->first();
        return view('edit', $data);
        //$where = array('id' => $id);
        //$data['task_info'] = Task::where($where)->first();
        //return view('edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(TaskStoreRequest $request, Task $task)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
        ]);
        $update = ['name' => $request->name, 'description' => $request->description];
        $task->update($update);
        //Task::where('id',$id)->update($update);

        return Redirect::to('tasks')->with('success','Great! Task updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        //Task::where('id',$id)->delete();
        $task->delete();
        return Redirect::to('tasks')->with('success','Task deleted successfully');
    }

}